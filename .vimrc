set nocompatible        " vi compatibility should be first line
let mapleader = ","     " remap Leader key

filetype plugin indent on

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" BASIC EDITING CONFIGURATION
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set backspace=indent,eol,start      " backspace compatibility for vi
set encoding=utf-8
set expandtab                       " use spaces instead of tabs
set fileencodings=utf-8
set hlsearch
set ignorecase
set incsearch
set laststatus=2
set list
set list listchars=tab:».,trail:·   " styling of tabs and spaces
set matchtime=2
set number                          " show line numbers in vim
set relativenumber
set ruler
set scrolloff=3
set sidescrolloff=5
set sidescroll=10
set textwidth=78
set cursorline
set showmatch
set smartcase
set tabstop=2
set autoindent
set splitbelow
set splitright

" Leader mappings
nmap <leader>v :edit $MYVIMRC<CR>
"noremap <Up> <NOP>
"noremap <Down> <NOP>
"noremap <Left> <NOP>
"noremap <Right> <NOP>
nnoremap t <C-]>  " follow link with 't' note: <C-t> goes back
nnoremap <CR> :nohlsearch<CR>/<BS>

" run pytests
nnoremap <leader>a :w\|:! python functional_test.py<CR>
nnoremap <leader>t :w\|:! pytest --flake8<CR>

if has('nvim')
  tnoremap <Esc> <C-\><C-n>
  tnoremap <C-v><Esc> <Esc>
endif

" let rst do make clean html for rst editing
nnoremap <F11> :make clean latexpdf<CR><CR><CR>
nnoremap <F12> :make clean html<CR><CR><CR>
nnoremap <C-p> :<C-u>FZF<CR>

" Navigation between split windows
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" MULTIPURPOSE TAB KEY
" Indent if we're at the beginning of a line. Else, do completion.
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
function! InsertTabWrapper()
    let col = col('.') - 1
    if !col || getline('.')[col - 1] !~ '\k'
        return "\<tab>"
    else
        return "\<c-p>"
    endif
endfunction
inoremap <expr> <tab> InsertTabWrapper()
inoremap <s-tab> <c-n>

" Grep
let g:grepper = {}
let g:grepper.tools = ['grep', 'git', 'ag', 'rg']
 " Open Grepper-prompt for a particular grep-alike tool
nnoremap <Leader>g :Grepper -tool git<CR>
nnoremap <Leader>G :Grepper -tool rg<CR>
" Search for the current word
nnoremap <Leader>* :Grepper -cword -noprompt<CR>
" Search for the current selection
nmap gs <plug>(GrepperOperator)
xmap gs <plug>(GrepperOperator)

function! SetupCommandAlias(input, output)
  exec 'cabbrev <expr> '.a:input
        \ .' ((getcmdtype() is# ":" && getcmdline() is# "'.a:input.'")'
        \ .'? ("'.a:output.'") : ("'.a:input.'"))'
endfunction
call SetupCommandAlias("grep", "GrepperRg")

" Commands
command! Q q            " Binds :Q to :q


" Automatically source vim cofig files after saving
if has("autocmd")
  autocmd bufwritepost .vimrc source $MYVIMRC
endif


" Always jump to the last known cursor position.
" Don't do it when the position is invalid or when inside
" an event handler (happens when dropping a file on gvim).
autocmd BufReadPost *
    \ if line("'\"") > 0 && line("'\"") <= line("$") |
    \ exe "normal g`\"" |
    \ endif


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" COLOR
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
syntax enable
set background=dark
set termguicolors
colorscheme one
" let g:solarized_termtrans=1
let g:airline_theme='one'

" Persistent Undo
set undofile
set undodir=~/.vim/undo
augroup vimrc
  autocmd!
  autocmd BufWritePre /tmp/* setlocal noundofile
augroup END

" Automatically resize splits when window is resized
au VimResized * exe "normal! \<c-w>="



" Packages
command! PackUpdate packadd minpac | source $MYVIMRC | redraw | call minpac#update()
command! PackClean  packadd minpac | source $MYVIMRC | call minpac#clean()

if !exists('*minpac#init')
  finish
endif

call minpac#init()

call minpac#add('k-takata/minpac', {'type': 'opt'})
call minpac#add('junegunn/fzf')
call minpac#add('mhinz/vim-grepper')
call minpac#add('prabirshrestha/asyncomplete.vim')
"call minpac#add('w0rp/ale')
call minpac#add('rakr/vim-one')
call minpac#add('icymind/NeoSolarized')
" neoterm
" vim-test
" webcomplete
