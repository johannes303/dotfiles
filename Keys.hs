module Keys where
  import XMonad
  import XMonad.Actions.CycleWS
  import XMonad.Actions.GridSelect

  -- additional key mappings
  customKeys	= 
    [ ((0, xK_Print), spawn "scrot -q 100")				-- capture screen
--    , ((controlMask, xK_Print), spawn "sleep 0.2; scrot -s -q 100")	-- choose window to capture
    , ((mod4Mask, xK_g), goToSelected defaultGSConfig)  -- ActionGridSelect
--    , ((mod4Mask, xK_a), spawn "usualapps")				-- launch mutt, firefox, and pidgin
--    , ((mod4Mask, xK_p), spawn "exe=`dmenu_path | dmenu -nb '#1c1c0e' -nf '#7d7d37' -sb 'gold' -sf 'grey30'` && eval \"exec $exe\"") -- dmenu
--    , ((mod4Mask, xK_Right), nextWS)					-- next workspace
--    , ((mod4Mask, xK_Left), prevWS)  					-- previous workspace
--    , ((mod4Mask, xK_F1), spawn "xmonad_keys.sh")			-- key help (toggle)
--    , ((mod4Mask, xK_F9), spawn "xscreensaver-command -lock && xset dpms force off") -- lock workstation and turn off display
    ]
